public class App {
    public static void main(String[] args) throws Exception {
        UsuarioBuilder objBuilder = new UsuarioBuilder();

        objBuilder = objBuilder.nombre("Juan");
        objBuilder = objBuilder.apellido("Hernán");
        objBuilder = objBuilder.email("Juan@gmail.com");
        objBuilder = objBuilder.telefono("123456");
        objBuilder = objBuilder.direccion("Cra 100");

        Usuario objUsuario = objBuilder.builder();

        System.out.println(objUsuario);

        Usuario objUsuario_2 = new UsuarioBuilder().nombre("Juan").apellido("Hernán").email("Juan@gmail.com").telefono("123456").direccion("Cra 100").builder();
        System.out.println(objUsuario_2);
    }
}
